﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace HW5
{
    // Xaml File Based off of docs Xamarin.Forms Map pins
    // https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/map/pins
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        ObservableCollection<Pin> pins;
        public MainPage()
        {
            InitializeComponent();
            pins = new ObservableCollection<Pin>();
            addPins();
        }

        // Adds pin details
        public void addPins()
        {
            Pin pin1 = new Pin
            {
                Type = PinType.Place,
                Address = "49275 Electron Dr, San Diego, CA 92152",
                Label = "Naval Base Point Loma",
                Position = new Position(32.708743, -117.247947)
            };

            Pin pin2 = new Pin
            {
                Type = PinType.Place,
                Address = "40466 Winchester Rd, Temecula, CA 92591",
                Label = "Socal Games and Comics",
                Position = new Position(33.528080, -117.151999)
            };

            Pin pin3 = new Pin
            {
                Type = PinType.Place,
                Address = "32115 Temecula Pkwy, Temecula, CA 92592",
                Label = "Augie's Coffee House",
                Position = new Position(33.479216, -117.098228)
            };

            Pin pin4 = new Pin
            {
                Type = PinType.Place,
                Address = "1403 Scott St, San Diego, CA 92106",
                Label = "Mitch's Seafood",
                Position = new Position(32.724088, -117.227256)
            };

            // adds pin locations to picker

            Lpicker.Items.Add("Naval Base Point Loma");
            Lpicker.Items.Add("Socal Games and Comics");
            Lpicker.Items.Add("Augie's Coffee House");
            Lpicker.Items.Add("Mitch's Seafood");

            // adds pin locations to map
            Lmap.Pins.Add(pin1);
            Lmap.Pins.Add(pin2);
            Lmap.Pins.Add(pin3);
            Lmap.Pins.Add(pin4);

        }

        // Handles picker selection
        private void Handle_SelectedIndexChange(object sender, EventArgs e)
        {
            var location = Lpicker.Items[Lpicker.SelectedIndex];
            if (location == "Naval Base Point Loma")
                Lmap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(32.708743, -117.247947), Distance.FromMiles(1)));
            if (location == "Socal Games and Comics")
                Lmap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.528080, -117.151999), Distance.FromMiles(1)));
            if (location == "Augie's Coffee House")
                Lmap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.479216, -117.098228), Distance.FromMiles(1)));
            if (location == "Mitch's Seafood")
                Lmap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(32.724088, -117.227256), Distance.FromMiles(1)));


        }

        // change to satellite view
        private void Handle_Sat(object sender, EventArgs e)
        {
            Lmap.MapType = MapType.Satellite;
        }


        //change to street view
        private void Handle_Str(object sender, EventArgs e)
        {
            Lmap.MapType = MapType.Street;
        }
    }
}
